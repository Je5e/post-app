package com.trainee.posts

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.trainee.posts.comment.CommentAdapter
import com.trainee.posts.network.Comment
import com.trainee.posts.network.Post
import com.trainee.posts.post.PostAdapter


@BindingAdapter("listData")
fun bindRecyclerView(
    recyclerView: RecyclerView,
    data: List<com.trainee.posts.domain.Post>?
) {
    val adapter = recyclerView.adapter as PostAdapter
    adapter.submitList(data)


}

@BindingAdapter("comments")
fun bindCommentsDat(
    recyclerView: RecyclerView,
    data: List<Comment>?
) {
    val adapter = recyclerView.adapter as CommentAdapter
    adapter.submitList(data)


}



package com.trainee.posts.comment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.trainee.posts.R
import com.trainee.posts.databinding.FragmentCommentListBinding
import com.trainee.posts.databinding.FragmentPostListBinding
import com.trainee.posts.post.PostAdapter
import com.trainee.posts.post.PostListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

@AndroidEntryPoint
class CommentListFragment : Fragment() {
    private var idPost by Delegates.notNull<Int>()


    private val viewModel: CommentListViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentCommentListBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        binding.commentList.adapter = CommentAdapter()
        return binding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            idPost = it.getInt("id")
        }
    }

}
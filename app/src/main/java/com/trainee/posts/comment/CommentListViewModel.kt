package com.trainee.posts.comment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.posts.database.comment.CommentRepository
import com.trainee.posts.network.PostsApi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class CommentListViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    val commentRepository: CommentRepository
) : ViewModel() {

    val postId: Int = savedStateHandle.get<Int>(POST_ID_SAVED_STATE_KEY)!!
    val comments = commentRepository.getCommentsByPostId(postId)


    init {
        refreshCommets()
    }

    private fun refreshCommets() {
        viewModelScope.launch {
            try {
                commentRepository.refreshComments()
            } catch (e: Exception) {

            }

        }
    }

    companion object {
        private const val POST_ID_SAVED_STATE_KEY = "id"
    }
}
package com.trainee.posts.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.trainee.posts.database.comment.Comment
import com.trainee.posts.database.comment.CommentDao
import com.trainee.posts.database.post.Post
import com.trainee.posts.database.post.PostDao

@Database(entities = arrayOf(Post::class, Comment::class), version = 1)

abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "app_database"
                ).build()
                INSTANCE = instance

                instance
            }
        }

    }

}

package com.trainee.posts.database.comment

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.trainee.posts.database.post.Post

@Entity(
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Post::class,
            parentColumns = arrayOf("id"), childColumns = arrayOf("postId"),
            onDelete = ForeignKey.CASCADE
        )
    )
)
data class Comment(
    @PrimaryKey val id: Int,
    @NonNull @ColumnInfo() val postId: Int,
    @NonNull @ColumnInfo(name = "name") val name: String,
    @NonNull @ColumnInfo(name = "email") val email: String,
    @NonNull @ColumnInfo(name = "body") val body: String
) {
}

/**
 * Map DatabaseVideos to domain entities
 */
fun List<Comment>.asDomainModel(): List<com.trainee.posts.network.Comment> {
    return map {
        com.trainee.posts.network.Comment(
            postId = it.postId,
            id = it.id,
            name = it.name,
            email=it.email,
            body = it.body
        )
    }
}
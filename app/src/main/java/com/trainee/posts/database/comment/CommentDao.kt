package com.trainee.posts.database.comment

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CommentDao {
    @Query("SELECT * FROM comment WHERE postId = :postId")
    fun getCommentsByPostId(postId: Int): LiveData<List<Comment>>

    @Query("SELECT * FROM comment")
    fun getAll(): LiveData<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(comments: List<Comment>)
}
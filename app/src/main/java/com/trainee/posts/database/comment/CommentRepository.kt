package com.trainee.posts.database.comment

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.trainee.posts.database.post.PostRepository
import com.trainee.posts.database.post.asDomainModel
import com.trainee.posts.domain.Post
import com.trainee.posts.network.Comment
import com.trainee.posts.network.PostsApiRepository
import com.trainee.posts.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommentRepository @Inject constructor(
    private val commentDao: CommentDao,
    private val postRepository: PostRepository,
    private val repositoryService:PostsApiRepository

) {

    val comments: LiveData<List<Comment>> = Transformations.map( commentDao.getAll()){
        it.asDomainModel()
    }
   fun getCommentsByPostId(id:Int) = Transformations.map( commentDao.getCommentsByPostId(id)){
       it.asDomainModel()
   }

    suspend fun refreshComments() {
        withContext(Dispatchers.IO) {
            val comments = repositoryService.getComments()
            commentDao.insertAll(comments.asDatabaseModel())
            }


        }
    }

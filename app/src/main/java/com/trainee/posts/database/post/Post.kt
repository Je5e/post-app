package com.trainee.posts.database.post

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NonNls

@Entity
data class Post(
    @PrimaryKey val id: Int,
    @NonNull @ColumnInfo(name = "post_title") val postTitle: String,
    @NonNull @ColumnInfo(name = "body") val body: String,
    @NonNull @ColumnInfo(name = "user_id") val userId: Int

)

/**
 * Map DatabaseVideos to domain entities
 */
fun List<Post>.asDomainModel(): List<com.trainee.posts.domain.Post> {
    return map {
        com.trainee.posts.domain.Post(
            userId = it.userId,
            id = it.id,
            title = it.postTitle,
            body = it.body
        )
    }
}

package com.trainee.posts.database.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.trainee.posts.domain.Post
import com.trainee.posts.network.PostsApiRepository
import com.trainee.posts.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostRepository @Inject constructor(
    private val postDao: PostDao,
    private val postRepositoryService: PostsApiRepository
) {
    val posts: LiveData<List<Post>> = Transformations.map( postDao.getAll()){
        it.asDomainModel()
    }


    /**
     * Refresh the videos stored in the offline cache.
     *
     * This function uses the IO dispatcher to ensure the database insert database operation
     * happens on the IO dispatcher. By switching to the IO dispatcher using `withContext` this
     * function is now safe to call from any thread including the Main thread.
     *
     */
    suspend fun refreshPosts() {
        withContext(Dispatchers.IO) {
            val posts = postRepositoryService.getPosts()
            postDao.insertAll(posts.asDatabaseModel())
        }
    }

}
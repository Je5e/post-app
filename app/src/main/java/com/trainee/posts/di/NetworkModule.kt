package com.trainee.posts.di

import com.trainee.posts.network.PostService
import com.trainee.posts.network.PostsApi
import com.trainee.posts.network.PostsApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providePostService(): PostsApiService {
        return PostsApi.retrofitService
    }
}
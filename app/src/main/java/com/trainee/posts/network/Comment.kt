package com.trainee.posts.network

class Comment(var postId: Int, var id: Int, var name: String, var email: String, var body: String) {
}

/**
 * Convert Network results to database objects
 */
fun List<Comment>.asDatabaseModel(): List<com.trainee.posts.database.comment.Comment> {
    return map {
        com.trainee.posts.database.comment.Comment(
            postId = it.postId,
            id = it.id,
            name = it.name,
            email = it.email,
            body = it.body
        )
    }
}
package com.trainee.posts.network

data class Post(val userId: Int, val id: Int, val title: String, val body: String)

/**
 * Convert Network results to database objects
 */
fun List<Post>.asDatabaseModel(): List<com.trainee.posts.database.post.Post> {
    return map {
        com.trainee.posts.database.post.Post(
            userId = it.userId,
            id = it.id,
            postTitle = it.title,
            body = it.body
        )
    }
}

package com.trainee.posts.network

import javax.inject.Inject
import javax.inject.Singleton


class PostsApiRepository @Inject constructor(private val postService: PostsApiService) {

    suspend fun getPosts() = postService.getPosts()
    suspend fun getCommentsByPostId(postId: Int) =
        postService.getCommentsByPostId(postId)

    suspend fun getComments() =
        postService.getComments()
}
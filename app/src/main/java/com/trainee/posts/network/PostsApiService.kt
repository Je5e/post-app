package com.trainee.posts.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

private const val BASE_URL =
    "https://jsonplaceholder.typicode.com/"
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()


interface PostsApiService {
    @GET("posts")
   suspend fun getPosts(): List<Post>

   @GET("comments")
   suspend fun getCommentsByPostId(@Query("postId") id:Int): List<Comment>
   @GET("comments")
   suspend fun getComments(): List<Comment>
}

 object PostsApi {

    val retrofitService: PostsApiService by lazy {
        retrofit.create(PostsApiService::class.java)
    }
}
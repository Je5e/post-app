package com.trainee.posts.post

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.trainee.posts.databinding.ListItemPostBinding
import com.trainee.posts.network.Post


class PostAdapter : ListAdapter<com.trainee.posts.domain.Post, PostAdapter.PostsViewHolder>(DiffCallback) {


    class PostsViewHolder(
        private var binding: ListItemPostBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.post?.let { post ->
                    navigateToPlant(post, it)
                }
            }
        }
        private fun navigateToPlant(
            post: com.trainee.posts.domain.Post,
            view: View
        ) {
            Log.d("Test","${post.id}")

            val direction =
                PostListFragmentDirections.actionPostListFragmentToCommentListFragment(
                    post.id
                )
            view.findNavController().navigate(direction)
        }
        fun bind(post: com.trainee.posts.domain.Post) {
            binding.post = post
            // This is important, because it forces the data binding to execute immediately,
            // which allows the RecyclerView to make the correct view size measurements
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<com.trainee.posts.domain.Post>() {
        override fun areItemsTheSame(oldItem: com.trainee.posts.domain.Post, newItem: com.trainee.posts.domain.Post): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: com.trainee.posts.domain.Post, newItem: com.trainee.posts.domain.Post): Boolean {
            return oldItem.title == newItem.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder(
            ListItemPostBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val post = getItem(position)
        holder.bind(post)
    }


}
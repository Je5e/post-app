package com.trainee.posts.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.posts.database.post.PostRepository
import com.trainee.posts.network.Post
import com.trainee.posts.network.PostsApi
import com.trainee.posts.network.PostsApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostListViewModel @Inject constructor(private val repository: PostRepository) : ViewModel() {

val posts = repository.posts

    init {
        RefreshDataFromRepository()
    }

    private fun RefreshDataFromRepository() {
        viewModelScope.launch {
            try {

                repository.refreshPosts()

            } catch (e: Exception) {

            }

        }
    }
}
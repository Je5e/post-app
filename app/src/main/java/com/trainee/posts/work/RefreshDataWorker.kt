package com.trainee.posts.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.trainee.posts.database.post.PostRepository
import retrofit2.HttpException
import javax.inject.Inject


class RefreshDataWorker @Inject constructor(
    appContext: Context, params: WorkerParameters,
    val repository: PostRepository
) :
    CoroutineWorker(appContext, params) {
    companion object {
        const val WORK_NAME = "com.trainee.posts.work.RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        try {
            repository.refreshPosts()
        } catch (e: HttpException) {
            return Result.retry()
        }
        return Result.success()

    }
}